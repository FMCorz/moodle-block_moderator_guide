<?php
// This file is part of Moderator Guide plugin for Moodle
//
// Moderator Guide plugin for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moderator Guide plugin for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moderator Guide plugin for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add/edit a guide.
 *
 * @package    block_moderator_guide
 * @copyright  2016 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 */

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/blocks/moderator_guide/forms.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/blocks/moderator_guide/locallib.php');

$action = optional_param('action', 'add', PARAM_ALPHA);

// If courseid is not passsed in the then this is and admin page.
$courseid = optional_param('courseid', 0, PARAM_INT);

// The preview mode is exclusively available from the admin.
$ispreview = ( $action == 'preview' );
if ($ispreview) {
    $courseid = 0;
}

// First things first.
if (empty($courseid)) {
    admin_externalpage_setup('block_moderator_guide_generic_admin_page');
} else {
    require_course_login($courseid);
}

// Check a template exists otherwise no need to go further send them back to manage guide where a proper message is displayed.
if (!block_moderator_guide_template_exists()) {
    redirect(new moodle_url('/blocks/moderator_guide/manage_guides.php', array('courseid' => $courseid, 'sesskkey' => sesskey())));
}

// Not really useful as anyway it is an admin page but just in case you want to move away from admin,
// then don't forget it ;).
require_capability('block/moderator_guide:editguide', $PAGE->context);

// Retrieve guide if edit mode.
$guide = new stdClass();
if ($action === 'delete') {
    require_sesskey();
    $guideid = required_param('id', PARAM_INT);
    $guide = $DB->get_record('block_mdrtr_guide_guides', array('id' => $guideid), '*', MUST_EXIST);

    block_moderator_guide_require_organisation_by_guide($guide);

    $confirm = optional_param('confirm', false, PARAM_INT);
    if ($confirm) {
        block_moderator_guide_delete_guide($guide);
        redirect(new moodle_url('/blocks/moderator_guide/manage_guides.php', array('courseid' => $courseid)));
    } else {
        // Display Delete Template confirmation page.
        $title = get_string('confirmdeleteguide', 'block_moderator_guide');

        block_moderator_guide_set_page($title, $courseid);

        echo $OUTPUT->header();
        $message = get_string('confirmdeleteguidetext', 'block_moderator_guide', $guide);
        $continue = new single_button(new moodle_url('/blocks/moderator_guide/edit_guide.php',
            array('action' => 'delete', 'sesskey' => sesskey(), 'id' => $guideid, 'confirm' => 1, 'courseid' => $courseid)),
            get_string('delete', 'block_moderator_guide'));
        $cancel = new single_button(new moodle_url('/blocks/moderator_guide/manage_guides.php', array('courseid' => $courseid)),
            get_string('cancel', 'block_moderator_guide'));
        echo $OUTPUT->confirm($message, $continue, $cancel);
        echo $OUTPUT->footer();
        die();
    }

} else if ($action === 'edit') {
    require_sesskey();

    // Find the guide and set the page title.
    $guideid = required_param('id', PARAM_INT);
    $guide = $DB->get_record('block_mdrtr_guide_guides', array('id' => $guideid), '*', MUST_EXIST);
    $title = get_string('editguide', 'block_moderator_guide');

    block_moderator_guide_require_organisation_by_guide($guide);

    // Show / Hide update + redirect to manage guides page.
    $hide = optional_param('hide', -1, PARAM_INT);
    if ($hide !== -1) {
        $guidetoedit = new stdClass();
        $guidetoedit->id = $guide->id;
        $guidetoedit->hidden = $hide;
        $DB->update_record('block_mdrtr_guide_guides', $guidetoedit);
        redirect(new moodle_url('/blocks/moderator_guide/manage_guides.php', array('courseid' => $courseid)));
    }

    // Set the guide contents.
    $forcetemplate = optional_param('forcetemplate', 0, PARAM_INT);
    if (empty($forcetemplate)) {
        $templateid = $guide->templateid;
    } else {
        $templateid = $forcetemplate;
    }
    $currenttemplate = $DB->get_record('block_mdrtr_guide_templates', array('id' => $templateid));
    $currenttemplatefields = block_moderator_guide_parse_template($currenttemplate->template);

    foreach ($currenttemplatefields as $fieldid => $field) {
        switch ($field['input']) {
            case 'html':
                $guidecontent = $DB->get_record('block_mdrtr_guide_contents',
                    array('guideid' => $guide->id, 'placeholderid' => $field['value']));

                // Check if guidecontent is empty (it happens when the template is forced - i.e. switch during edit mode).
                if (!empty($guidecontent)) {
                    $fieldname = 'field_' . $fieldid;
                    $fieldnameformat = 'field_' . $fieldid . 'format';
                    $guide->{$fieldname}['text'] = $guidecontent->value;
                    $guide->{$fieldnameformat}['format'] = $guidecontent->valueformat;
                }
                break;
            case 'files':
                // No need to the file manager value here - it is done by the file API.
                break;
            case 'link':
            case 'linkname':
                $guidecontent = $DB->get_record('block_mdrtr_guide_contents',
                    array('guideid' => $guide->id, 'placeholderid' => $field['value']));

                // Check if guidecontent is empty (it happens when the template is forced - i.e. switch during edit mode).
                if (!empty($guidecontent)) {

                    $fieldname = 'field_' . $fieldid;
                    $guide->{$fieldname} = $guidecontent->value;
                }
                break;
            default:
                break;
        }
    }

} else if ($ispreview) {
    $title = get_string('previewguide', 'block_moderator_guide');
    $forcetemplate = required_param('forcetemplate', PARAM_INT);
    $currenttemplate = $DB->get_record('block_mdrtr_guide_templates', array('id' => $forcetemplate), '*', MUST_EXIST);

    block_moderator_guide_require_organisation($currenttemplate);

    $currenttemplatefields = block_moderator_guide_parse_template($currenttemplate->template);
    $guide->action = 'preview';

} else {
    // This is a Add guide page.
    $title = get_string('addguide', 'block_moderator_guide');

    // Set the guide contents.
    $forcetemplate = optional_param('forcetemplate', 0, PARAM_INT);

    if (empty($forcetemplate)) {
        $formtemplateid = optional_param('template', 0, PARAM_INT);
        if (empty($formtemplateid)) {
            // Take the last template (same logic as in the forms.php file).
            $templates = $DB->get_records('block_mdrtr_guide_templates', array('hidden' => 0), '');
            while ($template = array_pop($templates)) {
                if (block_moderator_guide_can_see_template($template)) {
                    break;
                }
            }
            $templateid = $template->id;
        } else {
            $templateid = $formtemplateid;
        }
    } else {
        $templateid = $forcetemplate;
    }
    $currenttemplate = $DB->get_record('block_mdrtr_guide_templates', array('id' => $templateid));
    $currenttemplatefields = block_moderator_guide_parse_template($currenttemplate->template);

    block_moderator_guide_require_organisation($currenttemplate);

    $guide->name = $currenttemplate->defaultguidename;
}

block_moderator_guide_set_page($title, $courseid, $ispreview);

// Prepare file area.
if (!empty($guide->courseid)) {
    // We are editing.
    $coursecontext = context_course::instance($guide->courseid);
} else {
    // We are adding (can not be from the administration).
    $coursecontext = context_course::instance($COURSE->id);
}

$draftitemids = array();
foreach ($currenttemplatefields as $fieldid => $field) {
    if ($field['input'] === 'files') {
        $fieldname = 'field_' . $fieldid;
        $draftitemids[$fieldid] = file_get_submitted_draft_itemid($fieldname);
        file_prepare_draft_area($draftitemids[$fieldid], $coursecontext->id, 'block_moderator_guide',
            'filesplaceholder_' . $fieldid, empty($guide->id) ? null : $guide->id);
        $guide->{$fieldname} = $draftitemids[$fieldid];
    }
}

$form = new edit_guide_form($PAGE->url, $guide);

if ($form->is_cancelled()) {
    redirect(new moodle_url('/blocks/moderator_guide/manage_guides.php', array('courseid' => $courseid)));
} else if ($data = $form->get_data()) {
    // Creating new guide.
    $now = time();
    $newguide = new stdClass();
    $newguide->name = $data->name;
    $newguide->timemodified = $now;
    $newguide->templateid = $data->template;


    $template = $DB->get_record('block_mdrtr_guide_templates', array('id' => $newguide->templateid));
    $templatefields = block_moderator_guide_parse_template($template->template);

    // Set the courseid if we are in a course.
    if (!empty($courseid)) {
        $newguide->courseid = $courseid;
    }

    if (empty($data->id)) {
        $newguide->creatorid = $USER->id;
        $newguide->timecreated = $now;
        $newguide->id = $DB->insert_record('block_mdrtr_guide_guides', $newguide, true);

        // Insert content.
        $content = new stdClass();
        $content->guideid = $newguide->id;
        $content->timecreated = $now;
        $content->timemodified = $now;

        foreach ($templatefields as $fieldid => $field) {
            switch ($field['input']) {
                case 'html':
                    $fieldname = 'field_' . $fieldid;
                    $content->placeholderid = $field['value'];
                    $content->placeholdertype = $field['input'];
                    $content->value = $data->{$fieldname}['text'];
                    $content->valueformat = $data->{$fieldname}['format'];
                    $DB->insert_record('block_mdrtr_guide_contents', $content, true);
                    break;
                case 'files':
                    break;
                case 'link':
                case 'linkname':
                    $fieldname = 'field_' . $fieldid;
                    $content->placeholderid = $field['value'];
                    $content->placeholdertype = $field['input'];
                    $content->value = $data->{$fieldname};
                    $DB->insert_record('block_mdrtr_guide_contents', $content, true);
                    break;
                default:
                    break;
            }
        }

    } else {
        $newguide->id = $data->id;
        $DB->update_record('block_mdrtr_guide_guides', $newguide);

        // Update content.
        foreach ($templatefields as $fieldid => $field) {
            switch ($field['input']) {
                case 'html':
                    $fieldname = 'field_' . $fieldid;
                    $dbcontent = $DB->get_record('block_mdrtr_guide_contents',
                        array('guideid' => $data->id, 'placeholderid' => $field['value']));
                    $dbcontent->timemodified = $now;
                    $dbcontent->value = $data->{$fieldname}['text'];
                    $dbcontent->valueformat = $data->{$fieldname}['format'];
                    $DB->update_record('block_mdrtr_guide_contents', $dbcontent, true);
                    break;
                case 'files':
                    break;
                case 'link':
                case 'linkname':
                    $fieldname = 'field_' . $fieldid;
                    $dbcontent = $DB->get_record('block_mdrtr_guide_contents',
                        array('guideid' => $data->id, 'placeholderid' => $field['value']));
                    $dbcontent->timemodified = $now;
                    $dbcontent->value = $data->{$fieldname};
                    $DB->update_record('block_mdrtr_guide_contents', $dbcontent, true);
                    break;
                default:
                    break;
            }
        }
    }

    // Update the files.
    foreach ($currenttemplatefields as $fieldid => $field) {
        if ($field['input'] === 'files') {
            $fieldname = 'field_' . $fieldid;
            $info = file_get_draft_area_info($data->{$fieldname});
            $present = ($info['filecount'] > 0) ? '1' : '';
            file_save_draft_area_files($data->{$fieldname}, $coursecontext->id, 'block_moderator_guide',
                'filesplaceholder_' . $fieldid, $newguide->id);
        }
    }

    redirect(new moodle_url('/blocks/moderator_guide/manage_guides.php', array('courseid' => $courseid)));
}

$PAGE->requires->jquery();
$PAGE->requires->js('/blocks/moderator_guide/edit_guide.js');

echo $OUTPUT->header();
$form->display();
echo $OUTPUT->footer();

/**
 * Set the $PAGE navbar/crumtrail, the heading, the title and the url.
 *
 * @param string $title the page title.
 * @param int $courseid The course ID.
 * @param bool $preview Whether the page is in preview mode.
 */
function block_moderator_guide_set_page($title, $courseid, $preview = false) {
    global $PAGE, $COURSE;

    if (empty($courseid)) {
        // Set-up the page.
        $PAGE->set_heading($title);
        $PAGE->set_url(new moodle_url('/blocks/moderator_guide/edit_guide.php'));

        if ($preview) {
            $PAGE->navbar->add(get_string('managetemplates', 'block_moderator_guide'),
                new moodle_url('/blocks/moderator_guide/manage_templates.php'));
            $PAGE->navbar->add($title);
        } else {
            $PAGE->navbar->add(get_string('manageguides', 'block_moderator_guide'),
                new moodle_url('/blocks/moderator_guide/manage_guides.php'));
            $PAGE->navbar->add($title);
        }
    } else {
        $PAGE->set_pagelayout('incourse');
        $PAGE->navbar->add(get_string('pluginname', 'block_moderator_guide'));
        $PAGE->navbar->add(get_string('manageguides', 'block_moderator_guide'),
            new moodle_url('/blocks/moderator_guide/manage_guides.php', array('courseid' => $courseid)));
        $PAGE->navbar->add($title);
        $PAGE->set_heading($COURSE->fullname);
    }

    $PAGE->set_title($title);
    $guideid = optional_param('id', 0, PARAM_INT);
    $action = optional_param('action', 'add', PARAM_ALPHA);
    $PAGE->set_url(new moodle_url('/blocks/moderator_guide/edit_guide.php',
        array('courseid' => $courseid, 'id' => $guideid, 'action' => $action, 'sesskey' => sesskey())));
}
