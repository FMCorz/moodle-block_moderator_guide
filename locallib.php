<?php
// This file is part of Moderator Guide plugin for Moodle
//
// Moderator Guide plugin for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moderator Guide plugin for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moderator Guide plugin for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moderator Guide local library
 *
 * @package    block_moderator_guide
 * @copyright  2016 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Parse the content of a template.
 *
 * For example:
 *
 *   <h3>Grading advices for external grader</h3>
 *   <p>if you are an external grader this document will help you to know how to grade this course.</p>
 *   <p>You can know more about <a href="http://www.google.com">UK grading requirement</a> on the government site.</p>
 *   <p>[1:html]</p>
 *   <p>We recommend you read these following files:</p>
 *   <p>[2:files]</p>
 *   <p><br /></p>
 *
 * will become:
 *
 *  Array
 *    (
 *        [0] =>
 *     <h3>Grading advices for external grader</h3>
 *     <p>if you are an external grader this document will help you to know how to grade this course.</p>
 *     <p>You can know more about <a href="http://www.google.com">UK grading requirement</a> on the government site.</p>
 *     <p>
 *        [1] => [1:html]
 *        [2] => </p>
 *     <p>We recommend you read these following files:</p>
 *     <p>
 *        [3] => [2:files]
 *        [4] => </p>
 *     <p><br /></p>
 *
 *    )
 *
 * @param  string $templatecontent The content.
 * @return array
 */
function block_moderator_guide_parse_template($templatecontent) {

    $parsedfields = preg_split('/(\[[0-9]*:html\]|\[[0-9]*:files\]|\[[0-9]*:link\]|[[0-9]*:html:BEGIN\]|[[0-9]*:html:END\])/',
        $templatecontent, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

    $defaulthtmlplaceholder = false;
    $fields = array();
    $defaulthtml = '';
    foreach ($parsedfields as $id => $field) {
        switch ($field) {
            case (preg_match('/\[[0-9]*:html\]/', $field) ? true : false):
                if ($defaulthtmlplaceholder) {
                    // It is just a default html value [X:html] inside a [Y:html:BEGIN] (should not happen,
                    // it is likely a user error).
                    $defaulthtml .= $field;
                } else {
                    $fields[$id] = array('input' => 'html', 'value' => $field, 'id' => $id);
                }
                break;
            case (preg_match('/\[[0-9]*:files\]/', $field) ? true : false):
                if ($defaulthtmlplaceholder) {
                    // It is just a default html value [X:files] inside a [Y:html:BEGIN] (should not happen,
                    // it is likely a user error).
                    $defaulthtml .= $field;
                } else {
                    $fields[$id] = array('input' => 'files', 'value' => $field, 'id' => $id);
                }

                break;
            case (preg_match('/\[[0-9]*:link\]/', $field) ? true : false):
                if ($defaulthtmlplaceholder) {
                    // It is just a default html value [X:link] inside a [Y:html:BEGIN] (should not happen,
                    // it is likely a user error).
                    $defaulthtml .= $field;
                } else {
                    $fields[$id] = array('input' => 'link', 'value' => $field, 'id' => $id);
                    $fields[$id . '_linkname'] = array('input' => 'linkname',
                        'value' => $field . '_linkname', 'id' => $id . '_linkname');
                }

                break;
            case (preg_match('/[[0-9]*:html:BEGIN\]/', $field) ? true : false):
                if ($defaulthtmlplaceholder) {
                    // It is just a default html value [X:html:BEGIN] inside a [Y:html:BEGIN] (should not happen,
                    // it is likely a user error).
                    $defaulthtml .= $field;
                } else {
                    // Starting to record the default html till we reach a END placeholder.
                    $defaulthtml = '';
                    $defaulthtmlid = $id;
                    $defaulthtmlvalue = $field;
                    $defaulthtmlplaceholder = true;
                }
                break;
            case (preg_match('/[[0-9]*:html:END\]/', $field) ? true : false):
                if ($defaulthtmlplaceholder) {
                    $defaulthtmlplaceholder = false;
                    $fields[$defaulthtmlid] = array('input' => 'html', 'value' => $defaulthtmlvalue, 'id' => $defaulthtmlid,
                        'default' => $defaulthtml);
                } else {
                    // It is just static html value [X:html:END] (should not happen, it is likely a user error).
                    $fields[$id] = array('input' => 'static', 'value' => $field, 'id' => $id);
                }

                break;
            default:
                if ($defaulthtmlplaceholder) {
                    // Should be typical case of inside html between [X:html:BEGIN] and [Y:html:BEGIN].
                    $defaulthtml .= $field;
                } else {
                    // Just pure HTML outside placeholder.
                    $fields[$id] = array('input' => 'static', 'value' => $field, 'id' => $id);
                }
                break;
        }
    }

    return $fields;
}

/**
 * Check if at least 1 template is available to the $USER.
 *
 * @return bool
 */
function block_moderator_guide_template_exists() {
    global $USER, $CFG, $DB;

    // Retrieve user custom profile (if not admin).
    $restrictionfield = get_config('block_moderator_guide', 'restriction');
    $hasrestrictionfield = !empty($restrictionfield);
    $userfield = '';

    if (!is_siteadmin($USER) && $hasrestrictionfield) {
        require_once($CFG->dirroot . '/user/profile/lib.php');
        profile_load_data($USER);
        $userfield = !empty($USER->profile[$restrictionfield]) ? $USER->profile[$restrictionfield] : '';
    }

    // Build the query.
    $sql = "SELECT COUNT('x')
              FROM {block_mdrtr_guide_templates} t
             WHERE hidden = :hidden";
    $params = array('hidden' => 0);

    if ($hasrestrictionfield && !empty($userfield)) {
        // When the user has set their field, and there is a field restriction, then we filter.
        // See the function block_moderator_guide_can_see_template.
        $sql .=  " AND (t.organization = '' OR t.organization = :org)";
        $params['org'] = $userfield;
    }

    return $DB->count_records_sql($sql, $params) > 0;
}

/**
 * Delete a guide and associated data.
 *
 * @param  stdClass $guide The guide.
 * @return void
 */
function block_moderator_guide_delete_guide(stdClass $guide) {
    global $DB;

    // Delete the guide.
    $DB->delete_records('block_mdrtr_guide_guides', array('id' => $guide->id));

    // Delete all guide contents.
    $DB->delete_records('block_mdrtr_guide_contents', array('guideid' => $guide->id));

    // Delete all file area for this guide.
    $fs = get_file_storage();
    if (!empty($guide->id)) { // Little protection in case of bug, we never know...
        $fs->delete_area_files(context_course::instance($guide->courseid)->id, 'block_moderator_guide', false, $guide->id);
    }
}

/**
 * Delete a template and associated data.
 *
 * @param  int $templateid The template ID.
 * @return void
 */
function block_moderator_guide_delete_template($templateid) {
    global $DB;

    // Delete existing guides using this template.
    $guides = $DB->get_records('block_mdrtr_guide_guides', array('templateid' => $templateid));
    if (!empty($guides)) {

        $guideids = array_keys($guides);

        // Delete guides.
        $DB->delete_records_list('block_mdrtr_guide_guides', 'id', $guideids);

        // Delete all guide contents.
        $DB->delete_records_list('block_mdrtr_guide_contents', 'guideid', $guideids);

        // Delete all file areas for the guides.
        $fs = get_file_storage();
        foreach ($guideids as $guideid) {
            if (!empty($guideid)) { // Little protection in case of bug, we never know...
                $fs->delete_area_files(context_course::instance($guides[$guideid]->courseid)->id,
                    'block_moderator_guide', false, $guideid);
            }
        }
    }

    // Finally delete the template.
    $DB->delete_records('block_mdrtr_guide_templates', array('id' => $templateid));
}

/**
 * Return whether the current $USER can see a template.
 *
 * This does not perform capability checks, only asserts that the 'organization' of the
 * template matches the organization of the user. Or either are empty.
 *
 * @param  stdClass $template The template object.
 * @return bool
 */
function block_moderator_guide_can_see_template(stdClass $template) {
    global $CFG, $USER;

    // Admins can view everything.
    if (is_siteadmin($USER)) {
        return true;
    }

    // Users can view all the templates without organizations.
    if (empty($template->organization)) {
        return true;
    }

    // If we do not have a restriction field, return true.
    $restrictionfield = get_config('block_moderator_guide', 'restriction');
    if (empty($restrictionfield)) {
        return true;
    }

    // Preload the profile fields.
    require_once($CFG->dirroot . '/user/profile/lib.php');
    profile_load_data($USER);

    // If the user did not fill in their profile field, they can see everything.
    if (empty($USER->profile[$restrictionfield])) {
        return true;
    }

    return strtolower($USER->profile[$restrictionfield]) === strtolower($template->organization);
}

/**
 * Throw an exception if a template does not meet the organization requirement.
 *
 * @param  stdClass $template The template
 */
function block_moderator_guide_require_organisation(stdClass $template) {
    if (!block_moderator_guide_can_see_template($template)) {
        throw new moodle_exception('cannotmanagetemplate');
    }
}

/**
 * Throw an exception if a guide does not meet the organization requirement.
 *
 * @param  stdClass $guide The guide.
 */
function block_moderator_guide_require_organisation_by_guide(stdClass $guide) {
    global $DB;
    $template = $DB->get_record('block_mdrtr_guide_templates', array('id' => $guide->templateid));
    if (!block_moderator_guide_can_see_template($template)) {
        throw new moodle_exception('cannotmanageguide');
    }
}
