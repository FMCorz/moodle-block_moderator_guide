<?php
// This file is part of Moderator Guide plugin for Moodle
//
// Moderator Guide plugin for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moderator Guide plugin for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moderator Guide plugin for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * View a guide.
 *
 * @package    block_moderator_guide
 * @copyright  2016 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 */

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/blocks/moderator_guide/locallib.php');

// If courseid is not passsed in the then this is and admin page.
$guideid = required_param('guideid', PARAM_INT);
$guide = $DB->get_record('block_mdrtr_guide_guides', array('id' => $guideid));

require_course_login($guide->courseid);
require_capability('block/moderator_guide:viewguide', $PAGE->context);

// This is a Add guide page.
$title = $guide->name;

$PAGE->set_pagelayout('incourse');
$PAGE->set_url(new moodle_url('/blocks/moderator_guide/view.php', array('guideid' => $guideid)));
$PAGE->navbar->add(get_string('pluginname', 'block_moderator_guide'));
$PAGE->navbar->add($title);
$PAGE->set_heading($title);
$PAGE->set_title($title);

$template = $DB->get_record('block_mdrtr_guide_templates', array('id' => $guide->templateid));

// Check if the user can see the template.
if (!block_moderator_guide_can_see_template($template)) {
    throw new moodle_exception('sorry you have not the permission to access this template');
}

echo $OUTPUT->header();

// Add edit guide button if the user can edit it.
if (has_capability('block/moderator_guide:editguide', $PAGE->context) && block_moderator_guide_can_see_template($template)) {
    $editurl = new moodle_url('/blocks/moderator_guide/edit_guide.php',
        array('courseid' => $COURSE->id, 'sesskey' => sesskey(), 'action' => 'edit', 'id' => $guideid));
    echo $OUTPUT->single_button($editurl, get_string('editguide', 'block_moderator_guide'), 'post',
        array('class' => 'singlebutton block_moderator_guide_right'));
}

$templatefields = block_moderator_guide_parse_template($template->template);
$links = array();
foreach ($templatefields as $fieldid => $field) {
    switch ($field['input']) {
        case 'html':
            $html = $DB->get_record('block_mdrtr_guide_contents',
                array('guideid' => $guide->id, 'placeholderid' => $field['value']));
            echo html_writer::span($html->value);

            break;
        case 'files':
            $coursecontextid = context_course::instance($COURSE->id)->id;
            $fs = get_file_storage();
            $files = $fs->get_area_files($coursecontextid, 'block_moderator_guide',
                'filesplaceholder_' . $fieldid, $guide->id);
            foreach ($files as $file) {
                if ($file->get_filename() !== '.' && $file->get_filename() !== '..') {
                    $fileurl = moodle_url::make_pluginfile_url($coursecontextid, 'block_moderator_guide',
                        'filesplaceholder_' . $fieldid, $guide->id, '/', $file->get_filename());
                    echo html_writer::link($fileurl, $file->get_filename()) . '<br/>';
                }
            }
            break;
        case 'link':
            // Store the link, the link html will be created once we match linkname.
            $link = $DB->get_record('block_mdrtr_guide_contents',
                array('guideid' => $guide->id, 'placeholderid' => $field['value']));
            $links[$link->placeholderid . '_linkname'] = $link->value; // Store the url.

            break;
        case 'linkname':
            $linkname = $DB->get_record('block_mdrtr_guide_contents',
                array('guideid' => $guide->id, 'placeholderid' => $field['value']));
            echo html_writer::link(new moodle_url($links[$linkname->placeholderid]), $linkname->value);

            break;
        case 'static':
            $field['value'] = file_rewrite_pluginfile_urls($field['value'], 'pluginfile.php',
                $PAGE->context->id, 'block_moderator_guide', 'template', $guide->templateid);
            echo html_writer::span($field['value']);
            break;
        default:
            throw new coding_exception('template placeholder type unkown');
            break;
    }
}

echo $OUTPUT->footer();
