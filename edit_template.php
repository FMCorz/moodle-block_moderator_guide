<?php
// This file is part of Moderator Guide plugin for Moodle
//
// Moderator Guide plugin for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moderator Guide plugin for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moderator Guide plugin for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add/edit a template
 *
 * @package    block_moderator_guide
 * @copyright  2016 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 */

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/blocks/moderator_guide/forms.php');
require_once($CFG->dirroot . '/blocks/moderator_guide/locallib.php');
require_once($CFG->libdir . '/adminlib.php');

$action = optional_param('action', 'add', PARAM_ALPHA);
$confirm = optional_param('confirm', false, PARAM_INT);

admin_externalpage_setup('block_moderator_guide_generic_admin_page');

// Not really useful as anyway it is an admin page but just in case you want to move away from admin,
// then don't forget it ;).
require_capability('block/moderator_guide:edittemplate', $PAGE->context);

// Retrieve template if edit mode.
$template = new stdClass();
if ($action === 'delete') {
    require_sesskey();
    $templateid = required_param('id', PARAM_INT);

    if ($confirm) {
        block_moderator_guide_delete_template($templateid);
        redirect(new moodle_url('/blocks/moderator_guide/manage_templates.php'));

    } else {
        // Display Delete Template confirmation page.
        $template = $DB->get_record('block_mdrtr_guide_templates', array('id' => $templateid));
        $title = get_string('confirmdeletetemplate', 'block_moderator_guide');

        block_moderator_guide_set_page($title);

        echo $OUTPUT->header();
        $message = get_string('confirmdeletetemplatetext', 'block_moderator_guide', $template);

        // Check for existing guides using this template.
        $guides = $DB->get_records('block_mdrtr_guide_guides', array('templateid' => $templateid));
        if (!empty($guides)) {
            $message .= '<br/><br/><strong>WARNING: you are also going to delete the following guides:</strong><br/><ul>';
            foreach ($guides as $guide) {
                $message .= '<li>' .
                    html_writer::link(new moodle_url('/blocks/moderator_guide/view.php', array('guideid' => $guide->id)),
                    $guide->name) . '</li>';
            }
            $message .= '</ul>';
        }

        $continue = new single_button(new moodle_url('/blocks/moderator_guide/edit_template.php',
            array('action' => 'delete', 'sesskey' => sesskey(), 'id' => $templateid, 'confirm' => 1)),
            get_string('delete', 'block_moderator_guide'));
        $cancel = new single_button(new moodle_url('/blocks/moderator_guide/manage_templates.php'),
            get_string('cancel', 'block_moderator_guide'));
        echo $OUTPUT->confirm($message, $continue, $cancel);
        echo $OUTPUT->footer();
        die();
    }
} else if ($action === 'edit') {
    require_sesskey();

    // Find the template and set the page title.
    $templateid = required_param('id', PARAM_INT);
    $template = $DB->get_record('block_mdrtr_guide_templates', array('id' => $templateid));
    $template->template = array('text' => $template->template, 'format' => $template->templateformat);
    $title = get_string('edittemplate', 'block_moderator_guide');

    // Show / Hide update + redirect to manage templates page.
    $hide = optional_param('hide', -1, PARAM_INT);
    if ($hide !== -1) {
        $templatetoedit = new stdClass();
        $templatetoedit->id = $template->id;
        $templatetoedit->hidden = $hide;
        $DB->update_record('block_mdrtr_guide_templates', $templatetoedit);
        redirect(new moodle_url('/blocks/moderator_guide/manage_templates.php'));
    }

} else {
    // This is a Add template page.
    $title = get_string('addtemplate', 'block_moderator_guide');
    // Some default value for template so user is not lost.
    $template->template['text'] = <<<EOT
<pre><i>This is an example, feel free to edit as you want.</i></pre><h3>Grading advices for external grader</h3>
 <p>if you are an external grader this document will help you to know how to grade this course.</p>
 <pre><i>The following line is a editor placeholder.
It will be replaced by a Moodle HTML editor in the "Add/Edit guide" page:</i></pre>
 <p>[1:html]</p><p><br></p><p>You must check these requirements:</p><pre>The following lines are also an editor placeholder,
however this more complex placeholder contains some default HTML (here requirements) that will be displayed inside the editor.
It helps you to give some examples to the teacher.</pre>
 <p>[2:html:BEGIN]</p><p></p><ul><li>requirements A<br></li><li><span style="color: rgb(51, 51, 51); font-family:
 &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal;
 font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px;
 text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;
 background-color: rgb(255, 255, 255); display: inline !important; float: none;">requirements</span> B</li>
 <li><span style="color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;
 font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal;
 orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;
 -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">requirements
 </span> C</li></ul><p></p><p>[2:html:END]</p><p><br></p><p>We recommend you read these following files:</p>
 <pre><i>The following line is a file manager placeholder.
It will be replaced by a Moodle file manager in the "Add/Edit guide" page:</i></pre>
 <p>[3:files]</p><p><br></p><p>UK gov References:</p><p></p><pre style="padding: 9.5px; font-family: Monaco, Menlo, Consolas,
 &quot;Courier New&quot;, monospace; font-size: 13px; color: rgb(51, 51, 51); border-radius: 4px; display: block;
 margin: 0px 0px 10px; line-height: 20px; word-break: break-all; word-wrap: break-word; white-space: pre-wrap;
 background-color: rgb(245, 245, 245); border: 1px solid rgba(0, 0, 0, 0.14902); font-style: normal;
 font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2;
 text-align: left; text-indent: 0px; text-transform: none; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;">
 <i>The following line is a link placeholder.
It will be replaced by 2 text fields (url and link name) in the "Add/Edit guide" page:</i></pre>[4:link]<p></p>
 <p><br>Thank You,<br>University of Science</p>
EOT;
    $template->template['format'] = 1;
}

// Set the page.
block_moderator_guide_set_page($title);

// Prepare the draft file area for the template.
$draftideditor = file_get_submitted_draft_itemid('template');
$contextsystem = context_system::instance();
$templateid = empty($templateid) ? null : $templateid;
$currenttemplate = file_prepare_draft_area($draftideditor, $contextsystem->id,
    'block_moderator_guide', 'template', $templateid,
    edit_template_form::editor_options($contextsystem, $templateid), $template->template);
$template->template['text'] = $currenttemplate['text'];
$template->template['itemid'] = $draftideditor;

$form = new edit_template_form($PAGE->url, $template);
if ($form->is_cancelled()) {
    redirect(new moodle_url('/blocks/moderator_guide/manage_templates.php'));

} else if ($data = $form->get_data()) {
    // Creating new template.
    $now = time();
    $newtemplate = new stdClass();
    $newtemplate->name = $data->name;
    $newtemplate->defaultguidename = $data->defaultguidename;
    $newtemplate->description = $data->description;
    $newtemplate->organization = $data->organization;
    $newtemplate->timecreated = $now;
    $newtemplate->timemodified = $now;

    $dontupdatetemplate = optional_param('dontupdatetemplate', 0, PARAM_INT);
    if (empty($dontupdatetemplate)) {
        $newtemplate->template = $data->template['text'];
        $newtemplate->templateformat = $data->template['format'];
    }

    if (empty($data->id)) {
        $newtemplate->id = $DB->insert_record('block_mdrtr_guide_templates', $newtemplate, true);
    } else {
        $newtemplate->id = $data->id;
        $DB->update_record('block_mdrtr_guide_templates', $newtemplate);
    }

    if (empty($dontupdatetemplate)) {
        $newtemplate->template = file_save_draft_area_files($draftideditor, $contextsystem->id,
            'block_moderator_guide', 'template', $newtemplate->id,
            edit_template_form::editor_options($contextsystem, $newtemplate->id), $newtemplate->template);
        $DB->set_field('block_mdrtr_guide_templates', 'template', $newtemplate->template, array('id' => $newtemplate->id));
    }

    redirect(new moodle_url('/blocks/moderator_guide/manage_templates.php'));
}

echo $OUTPUT->header();

$form->display();

echo $OUTPUT->footer();

/**
 * Set the $PAGE navbar/crumtrail, the heading, the title and the url.
 *
 * @param string $title the page title.
 */
function block_moderator_guide_set_page($title) {
    global $PAGE;
    $PAGE->set_heading($title);
    $PAGE->set_title($title);
    $PAGE->set_url(new moodle_url('/blocks/moderator_guide/edit_template.php'));
    $PAGE->navbar->add(get_string('managetemplates', 'block_moderator_guide'),
        new moodle_url('/blocks/moderator_guide/manage_templates.php'));
    $PAGE->navbar->add($title);
}
