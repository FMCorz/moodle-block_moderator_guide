<?php
// This file is part of Moderator Guide plugin for Moodle
//
// Moderator Guide plugin for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moderator Guide plugin for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moderator Guide plugin for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package    block_moderator_guide
 * @category   string
 * @copyright  2016 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 */

defined('MOODLE_INTERNAL') || die();

$string['actions'] = 'Actions';
$string['addguide'] = 'Add guide';
$string['addguidehelp'] = 'To add a guide use the Moderator Guide block from any course main page.';
$string['addtemplate'] = 'Add template';
$string['author'] = 'Original author';
$string['cancel'] = 'Cancel';
$string['configtitle'] = 'Title';
$string['confirmdeleteguide'] = 'Delete guide confirmation';
$string['confirmdeleteguidetext'] = 'Do you really want to delete this guide: {$a->name} ?';
$string['confirmdeletetemplate'] = 'Delete template confirmation';
$string['confirmdeletetemplatetext'] = 'Do you really want to delete this template: {$a->name} ?';
$string['course'] = 'Course';
$string['defaultguidename'] = 'Default guide name';
$string['delete'] = 'Delete';
$string['description'] = 'Description';
$string['editguide'] = 'Edit guide';
$string['edittemplate'] = 'Edit template';
$string['guide'] = 'Guide';
$string['hide'] = 'Hide';
$string['manageguides'] = 'Manage guides';
$string['managetemplates'] = 'Manage templates';
$string['managetemplatesdesc'] = '<strong>Hide</strong>: the template will not be not available anymore to create the guides. However existing guides are still available.<br/>
<strong>Delete</strong>: it will delete the template and the guides using this template.<br/>
<strong>Edit</strong>: only the name, organization and description will be editable when a guide has been created with this template.<br/>
<strong>Guides</strong>: only available when a guide has been created with this template.';
$string['moderator_guide:addinstance'] = 'Can add an Moderator Guide block instance';
$string['moderator_guide:myaddinstance'] = 'Can add a Moderator Guide block instance on their dashboard';
$string['moderator_guide:editguide'] = 'Can edit an Moderator Guide guide';
$string['moderator_guide:edittemplate'] = 'Can edit an Moderator Guide template';
$string['moderator_guide:viewguide'] = 'Can view an Moderator Guide guide';
$string['moderator_guide:viewtemplate'] = 'Can view an Moderator Guide template';
$string['name'] = 'Name';
$string['noguidesforthiscourse'] = 'No guides for this course.';
$string['notemplate'] = 'No templates - your administrator must create and show at least one template first.';
$string['profilerestriction'] = 'Restrict by Custom Profile Field';
$string['profilerestrictiondesc'] = 'Restrict templates visible to teachers based on this field. Enter for example,
"organization" which is the shortname of a custom profile field. If the value of that field is empty then teachers will see all templates.
If it is populated, i.e. we enter "organization" in that field then the templates will be restricted by the organisation value (in each template)';
$string['organization'] = 'Organization';
$string['organizations'] = 'Organizations';
$string['pluginname'] = 'Moderator Guide';
$string['previewguide'] = 'Preview';
$string['sectionx'] = 'Section {$a->id}';
$string['show'] = 'Show';
$string['template'] = 'Template';
