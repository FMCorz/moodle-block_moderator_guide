<?php
// This file is part of Moderator Guide plugin for Moodle
//
// Moderator Guide plugin for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moderator Guide plugin for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moderator Guide plugin for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moderator Guide forms
 *
 * @package    block_moderator_guide
 * @copyright  2016 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/blocks/moderator_guide/locallib.php');

/**
 * Edit template form.
 *
 * @package    block_moderator_guide
 * @copyright  2016 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 */
class edit_template_form extends moodleform {

    /**
     * Form definition.
     */
    public function definition() {
        global $DB;

        $mform = $this->_form;
        $template = isset($this->_customdata) ? $this->_customdata : new stdClass();

        $mform->addElement('header', 'templateheader',
            get_string('template', 'block_moderator_guide'));

        $mform->addElement('text', 'name', get_string('name'));
        $mform->addRule('name', get_string('required'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        $mform->addElement('text', 'defaultguidename', get_string('defaultguidename', 'block_moderator_guide'));
        $mform->setType('defaultguidename', PARAM_TEXT);

        $mform->addElement('text', 'organization', get_string('organization', 'block_moderator_guide'), 'maxlength="255"');
        $mform->setType('organization', PARAM_TEXT);

        $mform->addElement('textarea', 'description', get_string('description', 'block_moderator_guide'),
            array('rows' => 8, 'cols' => 50));
        $mform->setType('description', PARAM_TEXT);

        $hasguides = false;
        if (!empty($template->id)) {
            $hasguides = $DB->count_records('block_mdrtr_guide_guides', array('templateid' => $template->id));
            if ($hasguides) {
                $mform->addElement('static', 'templatetext', get_string('template', 'block_moderator_guide'),
                    $template->template['text']);

                // TODO - use the moodleform function to try to put the correct value in template['text'],
                // TODO - instead of avoiding updating template.
                $mform->addElement('hidden', 'dontupdatetemplate', 1);
                $mform->setType('dontupdatetemplate', PARAM_INT);
                // TODO Use setConstant.
            }
        }

        if (empty($hasguides)) {
            $mform->addElement('editor', 'template', get_string('template', 'block_moderator_guide'),
                null, self::editor_options(context_system::instance(), (empty($template->id) ? null : $template->id)));
            $mform->setType('template', PARAM_CLEANHTML);
        }

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        if (!empty($template->id)) {
            $buttonlabel = get_string('savechanges');
        } else {
            $buttonlabel = get_string('addtemplate', 'block_moderator_guide');
        }

        $this->add_action_buttons(true, $buttonlabel);

        $this->set_data($template);
    }


    /**
     * Returns the options array to use in template text editor
     *
     * @param context_system $context
     * @param int $templateid post id, use null when adding new template
     * @return array
     */
    public static function editor_options(context_system $context, $templateid) {
        global $COURSE, $PAGE, $CFG;
        // TODO: add max files and max size support.
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => file_area_contains_subdirs($context, 'block_moderator_guide', 'template', $templateid)
        );
    }

}

/**
 * Guide edit form.
 *
 * @package    block_moderator_guide
 * @copyright  2016 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 */
class edit_guide_form extends moodleform {

    /**
     * Definition.
     */
    public function definition() {
        global $DB, $CFG, $PAGE, $COURSE, $USER;

        // Require for serving file.
        $coursecontext = context_course::instance($COURSE->id);

        $mform = $this->_form;
        $guide = isset($this->_customdata) ? $this->_customdata : new stdClass();

        $mform->addElement('header', 'guideheader',
            get_string('guide', 'block_moderator_guide'));

        $options = array();
        $forcetemplate = optional_param('forcetemplate', 0, PARAM_INT);
        if (!empty($guide->action) && $guide->action === 'preview') {
            $templates = $DB->get_records('block_mdrtr_guide_templates', array('id' => $forcetemplate));
        } else {
            $templates = $DB->get_records('block_mdrtr_guide_templates', array('hidden' => 0));
        }

        foreach ($templates as $template) {
            if (block_moderator_guide_can_see_template($template)) {
                if (empty($selectedtemplate)) {
                    // Force the template if found.
                    if (!empty($forcetemplate) && $forcetemplate == $template->id) {
                        $selectedtemplate = $template;
                        $guide->templateid = $template->id;
                        $mform->addElement('hidden', 'forcetemplate', $forcetemplate);
                        $mform->setType('forcetemplate', PARAM_INT);
                    }

                    // Retrieve the current template.
                    if (!empty($guide->templateid) && $template->id === $guide->templateid) {
                        $currenttemplate = $template;
                    }
                }
                $options[$template->id] = $template->name;
            }
        }

        // Set a template by default if none has been set.
        if (empty($selectedtemplate)) {
            if (!empty($currenttemplate)) {
                $selectedtemplate = $currenttemplate;
            } else {
                // The last template created (same as logic as in the edit_guide.php for adding a guide).
                $selectedtemplate = $template;
            }
        }

        $onchangeurl = $PAGE->url->out(false) . '&forcetemplate=';
        $mform->addElement('select', 'template', get_string('template', 'block_moderator_guide'), $options,
            array('changeurl' => $onchangeurl));
        $mform->setType('template', PARAM_INT);
        $mform->getElement('template')->setSelected($selectedtemplate->id);

        $mform->addElement('text', 'name', get_string('name'));
        $mform->addRule('name', get_string('required'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        // Create the template input fields.
        require_once($CFG->dirroot . '/blocks/moderator_guide/locallib.php');
        $templatefields = block_moderator_guide_parse_template($selectedtemplate->template);
        foreach ($templatefields as $field) {
            switch ($field['input']) {
                case 'html':
                    $mform->addElement('editor', 'field_' . $field['id'], '');
                    $mform->setType('field_' . $field['id'], PARAM_CLEANHTML);

                    // Set default.
                    $fieldname = 'field_' . $field['id'];
                    if (empty($guide->id) && !empty($field['default'])) {
                        $guide->{$fieldname}['text'] = $field['default'];
                    }
                    break;
                case 'files':
                    $mform->addElement('filemanager', 'field_' . $field['id'], '', null);
                    break;
                case 'link':
                    $mform->addElement('text', 'field_' . $field['id'], '', array('placeholder' => 'Link URL'));
                    $mform->setType('field_' . $field['id'], PARAM_URL);
                    break;
                case 'linkname':
                    $mform->addElement('text', 'field_' . $field['id'], '', array('placeholder' => 'Link Name'));
                    $mform->setType('field_' . $field['id'], PARAM_TEXT);
                    break;
                case 'static':
                    // Convert file urls.
                    $field['value'] = file_rewrite_pluginfile_urls($field['value'], 'pluginfile.php',
                        $coursecontext->id, 'block_moderator_guide', 'template', $selectedtemplate->id);
                    $mform->addElement('static', 'field_' . $field['id'], '', $field['value']);
                    break;
                default:
                    throw moodle_exception('type_unkown');
                    break;
            }
        }

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        if (!empty($guide->id)) {
            $buttonlabel = get_string('savechanges');
            $mform->setDefault('template', $guide->templateid);
        } else {
            $buttonlabel = get_string('addguide', 'block_moderator_guide');
        }

        if (empty($guide->action) || $guide->action !== 'preview') {
            $this->add_action_buttons(true, $buttonlabel);
        }

        $this->set_data($guide);
    }

}