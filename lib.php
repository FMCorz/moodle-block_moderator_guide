<?php
// This file is part of Moderator Guide plugin for Moodle
//
// Moderator Guide plugin for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moderator Guide plugin for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moderator Guide plugin for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moderator Guide lib
 *
 * @package    block_moderator_guide
 * @copyright  2016 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Serve the block files.
 *
 * @param object $course The course.
 * @param object $cm The course module.
 * @param context $context The context.
 * @param string $filearea The file area.
 * @param array $args Arguments.
 * @param bool $forcedownload Whether to force the download.
 * @param array $options Options.
 * @return bool
 */
function block_moderator_guide_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {

    // Make sure the filearea starts with filesplaceholder_.
    if ( (strpos($filearea, 'filesplaceholder_') !== 0) && ($filearea !== 'template') ) {
        return false;
    }

    // TODO Context we're checking in is different to context the file is in.
    require_capability('block/moderator_guide:viewguide', context_course::instance($course->id));

    // Change the context to system for template now that we check the user has view permission on the current guide.
    if ($filearea == 'template') {
        $context = context_system::instance();
    }

    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // When $args is empty => the path is '/'.
    } else {
        $filepath = '/' . implode('/', $args) . '/'; // Here $args contains elements of the filepath.
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();

    $file = $fs->get_file($context->id, 'block_moderator_guide', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    // Send the file.
    send_stored_file($file, null, 0, $forcedownload, $options);
}
